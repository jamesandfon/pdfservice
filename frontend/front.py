import requests
import json
import logging

from flask import Flask, render_template, request, redirect
app = Flask(__name__)


# URL = 'http://localhost:4875'
# python -m flask run --port=4875

LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)


@app.route('/')
def homepage():
    return render_template('homepage.html')

if __name__ == '__main__':
    app.run(debug = True)