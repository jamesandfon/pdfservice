#!/usr/bin/env python3
import os
import logging
import json
import uuid
import subprocess
import hashlib
import requests
import redis
import shutil
import time

from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,BucketAlreadyExists)

SERVICE_SERVER = 'http://pdfservice_queue-wrapper_1:5000/'
SOCKET_SERVER = 'http://pdfservice_server-side-rendering_1:4321/'

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
MINIO_URL = os.getenv("MINIO_URL", "localhost:9000")
QUEUE_NAME = 'queue:extracting'
INSTANCE_NAME = uuid.uuid4().hex

STATUS_OK = requests.codes['ok']

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

minioClient = Minio(MINIO_URL,
                    access_key='admin',
                    secret_key='password', 
                    secure=False)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def saveFile(log, ogTarUUID):
    allFiles = os.listdir("data/"+ogTarUUID)

    for file in allFiles:
        try:
            if not file.startswith('.'):
                with open("data/"+ogTarUUID+"/"+file, 'rb') as file_data:
                    file_stat = os.stat("data/"+ogTarUUID+"/"+file)
                    print(minioClient.put_object("upload", ogTarUUID+"/"+file,
                                        file_data, file_stat.st_size))
        except ResponseError as err:
            log.info("error...")
    shutil.rmtree('data/'+ogTarUUID)

def extract_file(log, task):
    
    try:
        ogTarUUID = task.get('ogTarUUID')
        ogTarName = task.get('ogTarName')
        task["status"] = "extracting"

        # info = {}
        # info["status"] = "extracting"
        # info["ogTarUUID"] = ogTarUUID
        # info["ogTarName"] = ogTarName
        
        r = requests.post(SERVICE_SERVER+'status', json=task)
        
        data = minioClient.get_object('upload', ogTarUUID+"/"+ogTarName)
        
        os.mkdir('data/'+ogTarUUID)
        with open('data/'+ogTarUUID+"/"+ogTarName, 'wb') as file_data:
            for d in data.stream(32*1024):
                file_data.write(d)
        subprocess.run(["./extract-service", ogTarUUID, ogTarName])
        saveFile(log, ogTarUUID)

        # r = requests.post(SOCKET_SERVER)
        
        r = requests.post(SERVICE_SERVER+'convert', json=json.dumps(task))
            
    except ResponseError as err:
        log.info(err)

    


def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    # subprocess.call(['mkdir', '-p', './'+VOLUME_NAME+'/'+INSTANCE_NAME])
    # os.mkdir("data/GG")
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')

    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: extract_file(named_logging, task_descr))

if __name__ == '__main__':
    main()
