#!/usr/bin/env python3
import os
import logging
import json
import uuid
import subprocess
import hashlib
import requests
import redis


LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
QUEUE_NAME = 'queue:extracting'
INSTANCE_NAME = uuid.uuid4().hex

VOLUME_NAME = 'data'
WORKER_FILE_STORAGE = './'+VOLUME_NAME+'/'+INSTANCE_NAME
VOLUME_PATH = '/app/'+VOLUME_NAME

STATUS_OK = requests.codes['ok']

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def execute_extracting(log, task):
    bucketname = task.get('bucketname')
    objectname = task.get('objectname')
    targetbucketname = task.get('targetbucketname')
    targetobjectname = task.get('targetobjectname')
    if bucketname and objectname and targetbucketname and targetobjectname:
        log.info('Execute thumbnailing: Bucketname %s, Objectname %s', bucketname, objectname)

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    subprocess.call(['mkdir', '-p', './'+VOLUME_NAME+'/'+INSTANCE_NAME])
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: execute_extracting(named_logging, task_descr))

if __name__ == '__main__':
    main()
