from flask import Flask, render_template, session, request, redirect, url_for
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect
from werkzeug import secure_filename
import requests
import logging
import json
import uuid
import os
from datetime import timedelta
# Import Minio library.
from pymongo import MongoClient
from minio import Minio
import curses
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,
                         BucketAlreadyExists)


MINIO_URL = os.getenv("MINIO_URL", "localhost:9000")
minioClient = Minio(MINIO_URL,
                    access_key='admin',
                    secret_key='password', 
                    secure=False)

mongoClient = MongoClient('mongodb://datastore:27017/dockerdemo')
db = mongoClient.appdb

SERVICE_SERVER = 'http://pdfservice_queue-wrapper_1:5000/'

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
LOG = logging
LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

clients =[]
STATUS_OK = requests.codes['ok']

def upload_file_to_minio(f, fileName, genUuid):
    try:
        LOG.info("doing fput")
        # fput_object(bucket_name, object_name, file_path, content_type="application/octet-stream", metadata=None)
        filePath ='./data/'+fileName
        minioClient.fput_object('upload', genUuid +'/'+fileName, filePath)
        os.remove(filePath)
    except ResponseError as err:
        LOG.info(err)

def make_bucket(f, fileName, genUuid):
    try:
        minioClient.make_bucket("upload")
    except ResponseError as err:
        LOG.info("response error")
        LOG.info(err)
        raise
    except BucketAlreadyOwnedByYou as err:
        LOG.info("bucketalreadyownedbyyou")
        LOG.info(err)
        upload_file_to_minio(f, fileName, genUuid)
        pass
    else:
        upload_file_to_minio(f, fileName, genUuid)
        
        
## HELPER
def upload(f, fileName):
    genUuid = uuid.uuid4().hex
    make_bucket(f, fileName,genUuid)
    return genUuid

## SOCKET
# @socketio.on('my event')
# def test_message(message):
#     LOG.info('my event socket')
#     emit('my response', {'data': message['data']})

# @socketio.on('my broadcast event')
# def test_broadcast_message(message):
#     emit('my response', {'data': message['data']}, broadcast=True)

@socketio.on('connect')
def test_connect():
    # emit('my response', {'data': 'Connected'})
    LOG.info(request.namespace+" connected")
    clients.append(request.namespace)

@socketio.on('join')
def join(message):
    room = message['room']
    LOG.info("join: "+room)
    join_room(room)


@socketio.on('my_room_event')
def send_room_message(message):
    LOG.info(message)
    emit('my response',
         {'data': message['data']}, room=message['room'])

@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected', request.sid)

## CONTROLLER APIS
@app.route('/findAll', methods = ['GET'])
def db_find_all():
    _items = db.appdb.find()
    items = [item for item in _items]
    LOG.info(items)
    return "ok"

@app.route('/deleteAll', methods = ['DELETE'])
def db_delete_all():
    _items = db.appdb.delete_many({})
    # LOG.info(_items.deleted_count, " documents deleted.")
    return "ok"

@app.route('/upload', methods = ['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        f = request.files['file']
        fileName =secure_filename(f.filename)
        f.save('./data/'+ fileName)
        genUuid = upload(f, fileName)

        # update status to mongo
        status = "waiting to be work on"
        item_doc = {
            'fileName' : fileName,
            'ogTarUUID': genUuid,
            'status': status
        }
        db.appdb.insert_one(item_doc)
        requests.post(SERVICE_SERVER +"extract", json=json.dumps({'ogTarUUID': genUuid, 'ogTarName': fileName}))
        return redirect(url_for('status', uuid=genUuid))
        # return render_template('socket.html', uuid=genUuid)

def genDownloadButton(uuid):
    myquery = {"ogTarUUID": uuid}
    dbdata = db.appdb.find(myquery)
    link=minioClient.presigned_get_object('download',uuid+'/'+dbdata[0]["fileName"], expires=timedelta(days=2))
    link="http://localhost:"+link.split(":")[2].split("?")[0]
    return("<div><p>Done. File are compressed</p></div><a href="+link+"><button>Download</button></a>")
        # return ("<h1>yo</h1>"+"<button type='submit'>Download!</button>")
    # return ("""<div><p>Done. File are compressed</p></div> <form class="form" action = "http://localhost:4321/download"""+"/" + room +"""" """ """ method = "GET" 
    #     enctype = "multipart/form-data">
    #         <p style="text-align: left; margin: 0px; font-weight: bold; color:black; margin-bottom: 5px">Download file here </p>
    #         <button type = "submit"> Download! </button>
    #     </form>
    # """)


@app.route('/req/<room>', methods = ['POST'])
def req(room):
    body = request.data.decode('utf-8')
    LOG.info(body)
    if body == "Done":
        socketio.emit('my response', 
            {'data': 
                genDownloadButton(room)
            }, 
            room=room)
    else:
        socketio.emit('my response', {'data': body}, room=room)
    return 'successful'
 
@app.route('/download/<uuid>', methods=['GET'])
def get_download(uuid):
    # Get a full object.
    # LOG.info(dbdata[0]["fileName"])
    # try:
    #     data = minioClient.get_object('download', uuid+'/'+dbdata[0]["fileName"])
    #     with open('my-testfile', 'wb') as file_data:
    #         for d in data.stream(32*1024):
    #             file_data.write(d)
    # except ResponseError as err:
    #     LOG.info(err)

    return "thankyou for using our service."

### HTML RENDERING APIS
@app.route('/')
def home():
    return render_template('index.html')

@app.route('/status/<uuid>')
def status(uuid):
    myquery = {"ogTarUUID": uuid}
    status = db.appdb.find(myquery)[0]["status"]
    LOG.info("status: "+status)
    if status == "Done":
        status = genDownloadButton(uuid)
    return render_template('socket.html', uuid=uuid, status=status)


