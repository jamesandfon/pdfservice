#!/usr/bin/env python3
import os
import logging
import json
import uuid
import subprocess
import hashlib
import requests
import redis
import shutil
import time
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,BucketAlreadyExists)

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
MINIO_URL = os.getenv("MINIO_URL", "localhost:9000")
QUEUE_NAME = 'queue:compressing'
INSTANCE_NAME = uuid.uuid4().hex
SOCKET_SERVER = 'http://pdfservice_server-side-rendering_1:4321'
SERVICE_SERVER = 'http://pdfservice_queue-wrapper_1:5000/'

STATUS_OK = requests.codes['ok']

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

minioClient = Minio(MINIO_URL,
                    access_key='admin',
                    secret_key='password', 
                    secure=False)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True
    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)
        if not packed:
            # if nothing is returned, poll a again
            continue
        _, packed_task = packed
        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def make_bucket(bucketname):
    try:
        minioClient.make_bucket(bucketname)
    except ResponseError as err:
        LOG.info("response error")
        LOG.info(err)
        raise
    except BucketAlreadyOwnedByYou as err:
        LOG.info("bucketalreadyownedbyyou")
        LOG.info(err)
        pass
    policy_read_only = {"Version":"2012-10-17",
                    "Statement":[
                        {
                        "Sid":"",
                        "Effect":"Allow",
                        "Principal":{"AWS":"*"},
                        "Action":"s3:GetBucketLocation",
                        "Resource":"arn:aws:s3:::{}".format(bucketname)
                        },
                        {
                        "Sid":"",
                        "Effect":"Allow",
                        "Principal":{"AWS":"*"},
                        "Action":"s3:ListBucket",
                        "Resource":"arn:aws:s3:::{}".format(bucketname)
                        },
                        {
                        "Sid":"",
                        "Effect":"Allow",
                        "Principal":{"AWS":"*"},
                        "Action":"s3:GetObject",
                        "Resource":"arn:aws:s3:::{}/*".format(bucketname)
                        }
                    ]}
    try:
        json_policy = json.dumps(policy_read_only)
        minioClient.set_bucket_policy(bucketname, json_policy)
    except ResponseError as err:
        LOG.info("ERRRRRRR")
        LOG.info(err)
        
def saveFile(log, ogTarUUID, ogTarName):
    try:
        make_bucket("download")
        with open('data/'+ogTarUUID+'/'+ogTarName, 'rb') as file_data:
            file_stat = os.stat('data/'+ogTarUUID+'/'+ogTarName)
            print(minioClient.put_object('download', ogTarUUID+'/'+ogTarName,
                                file_data, file_stat.st_size))
                
    except ResponseError as err:
        print(err)
    shutil.rmtree('data/'+ogTarUUID)

def deleteFiles(log, ogTarUUID):
    try:
        objects = minioClient.list_objects('upload',recursive=True)
        objects_to_delete = []

        for obj in objects:
            objectName = obj.object_name
            if (objectName.startswith(ogTarUUID) and objectName.endswith('.pdf') or objectName.endswith('txt')):
                folder, name = objectName.split('/')

                objects_to_delete.append(name)

        for obj in objects_to_delete:
            try:
                minioClient.remove_object('upload', ogTarUUID+'/'+obj)
            except ResponseError as err:
                print(err)

    except ResponseError as err:
        print(err)
        
def send_status(log, task):
    # info = {}
    # info["status"] = "extracting"
    # info["ogTarUUID"] = ogTarUUID
    # info["ogTarName"] = ogTarName
    # info["fileName"] = fileName

    # objects = minioClient.list_objects('upload',recursive=True)
    
    # total = len(list(objects))
    
    # task["total"] = total - 1
    task["status"] = "compressed"

    r = requests.post(SERVICE_SERVER+'status', json=task)

def execute_compressing(log, task):
    
    try:
        ogTarUUID = task.get('ogTarUUID')
        ogTarName = task.get('ogTarName')
        task["status"] = "compressing"

        r = requests.post(SERVICE_SERVER+'status', json=task)
        
        objects = minioClient.list_objects('upload',recursive=True)
        try:
            os.mkdir('data/'+ogTarUUID)
        except:
            log.info('cannot create directory')
        for obj in objects:
            objectName = obj.object_name
            if (objectName.startswith(ogTarUUID) and objectName.endswith('.txt')):
                folder, name = objectName.split('/')
                data = minioClient.get_object('upload', ogTarUUID+"/"+name)
                
                with open('data/'+ogTarUUID+'/'+name, 'wb') as file_data:
                    for d in data.stream(32*1024):
                        file_data.write(d)
        subprocess.run(["./compress-service", ogTarUUID, ogTarName])
        saveFile(log, ogTarUUID, ogTarName)

        task["status"] = "compressed"
        r = requests.post(SERVICE_SERVER+'status', json=task)
        
        log.info("========= done compressing")

        send_status(log, task)
        
    except ResponseError as err:
        log.info(err)
    

def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: execute_compressing(named_logging, task_descr))

if __name__ == '__main__':
    main()
