#!/usr/bin/env python3
import os
import logging
import json
import uuid
import subprocess
import hashlib
import requests
import redis
import shutil
from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,BucketAlreadyExists)

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
MINIO_URL = os.getenv("MINIO_URL", "localhost:9000")
QUEUE_NAME = 'queue:converting'
INSTANCE_NAME = uuid.uuid4().hex
SERVICE_SERVER = 'http://pdfservice_queue-wrapper_1:5000/'
SOCKET_SERVER = 'http://pdfservice_server-side-rendering_1:4321'

STATUS_OK = requests.codes['ok']

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

minioClient = Minio(MINIO_URL,
                    access_key='admin',
                    secret_key='password', 
                    secure=False)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)
        if not packed:
            # if nothing is returned, poll a again
            continue
        _, packed_task = packed
        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)
def saveFile(log, ogTarUUID):
    allFiles = os.listdir("data/"+ogTarUUID)

    for file in allFiles:
        try:
            if not file.startswith('.') and file.endswith('.txt'):
                with open("data/"+ogTarUUID+"/"+file, 'rb') as file_data:
                    file_stat = os.stat("data/"+ogTarUUID+"/"+file)
                    print(minioClient.put_object("upload", ogTarUUID+"/"+file,
                                        file_data, file_stat.st_size))
        except ResponseError as err:
            log.info("error...")
    shutil.rmtree('data/'+ogTarUUID)

def send_status(log, task):
    # info = {}
    # info["status"] = "extracting"
    # info["ogTarUUID"] = ogTarUUID
    # info["ogTarName"] = ogTarName
    # info["fileName"] = fileName
    ogTarName = task.get('ogTarName')
    ogTarUUID = task.get('ogTarUUID')

    objects = minioClient.list_objects('upload',recursive=True)
    total = 0

    for obj in objects:
            objectName = obj.object_name
            if (objectName.startswith(ogTarUUID) and objectName.endswith('.pdf')):
                total += 1
                # folder, name = objectName.split('/')
                # data = minioClient.get_object('upload', ogTarUUID+"/"+name)
                
                # with open('data/'+ogTarUUID+'/'+name, 'wb') as file_data:
                #     for d in data.stream(32*1024):
                #         file_data.write(d)
    
    # total = len(list(objects))
    
    task["total"] = str(total)
    task["status"] = "converting"

    log.info("task--------------")
    log.info(task)


    r = requests.post(SERVICE_SERVER+'status', json=task)



def execute_converting(log, task):
    log.info('Execute converting')
    
    # Get a full object.
    try:
        ogTarUUID = task.get('ogTarUUID')
        ogTarName = task.get('ogTarName')
        fileName = task.get('fileName')

        send_status(log, task)

        data = minioClient.get_object('upload', ogTarUUID+"/"+fileName)
        try:
            os.mkdir('data/'+ogTarUUID)
        except:
            pass
        with open('data/'+ogTarUUID+'/'+fileName, 'wb') as file_data:
            for d in data.stream(32*1024):
                file_data.write(d)
        subprocess.run(["./txtmaker-service", ogTarUUID, fileName])
        saveFile(log, ogTarUUID)

        # log.info(task)
        # data = {}
        # data["ogTarUUID"] = ogTarUUID
        # data["ogTarName"] = ogTarName

        # r = requests.post(SERVICE_SERVER+'compress', json=json.dumps(task))

        # r = requests.post(SERVICE_SERVER+'compress', json=json.dumps(task))
        # # log.info(r.status_code)
    except ResponseError as err:
        print(err)


def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: execute_converting(named_logging, task_descr))

if __name__ == '__main__':
    main()
