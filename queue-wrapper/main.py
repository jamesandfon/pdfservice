import os
import json
import requests
import redis
import logging
import subprocess
from flask import Flask, jsonify, request, Response

from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,BucketAlreadyExists)

app = Flask(__name__)
SERVICE_SERVER = 'http://localhost:5000/'
LOG = logging
MINIO_URL = os.getenv("MINIO_URL", "localhost:9000")

minioClient = Minio(MINIO_URL,
                    access_key='admin',
                    secret_key='password', 
                    secure=False)


LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
class RedisResource:
    REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
    
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def addWorkToQueue(queue_name,body):
    json_packed = json.dumps(body)
    RedisResource.conn.rpush(
        queue_name,
        json_packed)


# API that submits a job to the queue
@app.route('/extract', methods=['POST'])
def post_extract():
    # body = request.json
    body = json.loads(request.json)
    addWorkToQueue('queue:extracting', body)

    return jsonify({'status': 'OK'})

@app.route('/convert', methods=['POST'])
def post_convert():
    body = request.json    
    info = json.loads(body)
    ogTarUUID = info.get('ogTarUUID')
    
    try:
        objects = minioClient.list_objects('upload',recursive=True)
        # LOG.info("==== this is length")
        # LOG.info(len(list(objects())))
        # total = len(list(objects))
        # # info["total"] = total
        # LOG.info(total)
        # LOG.info(type(total))
        # LOG.info(objects)


        for obj in objects:
            objectName = obj.object_name
            if (objectName.startswith(ogTarUUID) and objectName.endswith('.pdf')):
                folder, name = objectName.split('/')
                info["fileName"] = name
                addWorkToQueue('queue:converting', info)
        
        
        return jsonify({'status': 'OK'})

    except ResponseError as err:
        print(err)
    return jsonify({'status': 'BAD REQUEST'}), 400

# API that submits a job to the queue
@app.route('/compress', methods=['POST'])
def post_compress():
    LOG.info("this is compressing")
    # body = request.json
    body = json.loads(request.json)
    addWorkToQueue('queue:compressing', body)
    return jsonify({'status': 'OK'})


@app.route('/status', methods=['POST']) 
def post_status():


    body = request.json
    # body = json.loads(body)

    LOG.info("===== ======")
    LOG.info(body)


    # body["status"] = status
    # body["ogTarUUID"] = ogTarUUID
    
    addWorkToQueue('queue:status', body)
    return jsonify({'status': 'OK'})