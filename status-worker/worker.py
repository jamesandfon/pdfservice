#!/usr/bin/env python3
import os
import logging
import json
import uuid
import subprocess
import hashlib
import requests
import redis
import time

from minio import Minio
from minio.error import (ResponseError, BucketAlreadyOwnedByYou,BucketAlreadyExists)

from pymongo import MongoClient

SERVICE_SERVER = 'http://pdfservice_queue-wrapper_1:5000/'
SERVER_SIDE_RENDERING = 'http://pdfservice_server-side-rendering_1:4321/'

LOG = logging
REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'localhost')
MINIO_URL = os.getenv("MINIO_URL", "localhost:9000")
QUEUE_NAME = 'queue:status'
INSTANCE_NAME = uuid.uuid4().hex

STATUS_OK = requests.codes['ok']

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

minioClient = Minio(MINIO_URL,
                    access_key='admin',
                    secret_key='password', 
                    secure=False)

mongoClient = MongoClient('mongodb://datastore:27017/dockerdemo')
db = mongoClient.appdb

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task.decode('utf8'))
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def update_db(status, ogTarUUID, newvalues):
    myquery = {"ogTarUUID": ogTarUUID}
    db.appdb.update_one(myquery, newvalues)

def emit_status(status, ogTarUUID):
    r = requests.post(SERVER_SIDE_RENDERING+'req/'+ogTarUUID, data=status)

def send_waiting_status(status, ogTarUUID, statusCode):
    #update status in mongo
    newvalues = {"$set": {"status": status, "statusCode": statusCode}}
    update_db(status, ogTarUUID,newvalues)
    emit_status(status, ogTarUUID)

def send_extract_status(status, ogTarUUID, statusCode):
    #update status in mongo
    newvalues = {"$set": {"status": status,"statusCode": statusCode, "converted": "0"}}
    update_db(status, ogTarUUID, newvalues)
    emit_status(status, ogTarUUID)

def send_convert_status(status, ogTarUUID, totalFile, statusCode):
    #get the number first
    myquery = {"ogTarUUID": ogTarUUID}
    dbdata = db.appdb.find(myquery)
    numconverted = dbdata[0]["converted"]
    LOG.info("numconverted "+numconverted)
    updatednumconverted = str(int(numconverted)+1)
    LOG.info("upnumcon "+ updatednumconverted)
    #update status in mongo
    newStatus = updatednumconverted +'/'+ totalFile+' '+status
    newvalues = {"$set": {"status": newStatus, "statusCode":statusCode ,"converted":updatednumconverted, "total":totalFile }}
    update_db(status, ogTarUUID, newvalues)
    emit_status(newStatus, ogTarUUID)

def send_compressing_status(status, ogTarUUID, statusCode):
    newValues = {"$set": {"status": status, "statusCode": statusCode}}
    update_db(status, ogTarUUID, newValues)
    emit_status(status, ogTarUUID)

def is_converting_done(ogTarUUID, total):
    myquery = {"ogTarUUID": ogTarUUID}
    dbdata = db.appdb.find(myquery)
    numconverted = dbdata[0]["converted"]
    # LOG.info("numconveted: ",numconverted)
    # LOG.info("total: ", str(total))
    return total == numconverted
def work_on_status(log, task): 
    log.info("this is worker status=======")
    log.info(task)
    statusCode = task.get("status")
    ogTarUUID = task.get("ogTarUUID")
    LOG.info("status: "+statusCode)
    LOG.info("ogTarUUID: "+ogTarUUID)
    if statusCode == "waiting":
        send_waiting_status("waiting to be work on", ogTarUUID, statusCode)
        pass
    elif statusCode == "extracting":
        LOG.info("sending extracting status")
        send_extract_status("files are being extracted", ogTarUUID, statusCode)
        pass
    elif statusCode == "converting":
        total = task.get('total')
        if total != None:
            send_convert_status("PDFs being converted", ogTarUUID, total, statusCode)
            if is_converting_done(ogTarUUID, total):
                # add compress work to work queue
                jsonToSend = {'ogTarUUID': ogTarUUID, 'ogTarName': task.get('ogTarName')}
                requests.post(SERVICE_SERVER+'compress', json=json.dumps(jsonToSend))

        pass
    elif statusCode == "compressing":
        # TODO:
        # if it converting is actually completed
        # check mongo to see if total is equal to converted?
        # if yes send compressing status.

        send_compressing_status("text files are being packed into an output tar ball", ogTarUUID, statusCode)
        pass
    elif statusCode == "compressed":
        # downloadLink = task.get("downloadLink")
        # if downloadLink != None:
        send_compressing_status("Done", ogTarUUID, statusCode)
        pass

    


def main():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    # subprocess.call(['mkdir', '-p', './'+VOLUME_NAME+'/'+INSTANCE_NAME])
    # os.mkdir("data/GG")
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')

    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        QUEUE_NAME,
        lambda task_descr: work_on_status(named_logging, task_descr))

if __name__ == '__main__':
    main()
